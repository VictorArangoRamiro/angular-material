'use strict';

angular.module('MyApp')
.service('countriesService', function ($http) 
{
    var urlCountries = 'https://restcountries.eu/rest/v2/all';
    
    this.getCountriesData = function ()
    {
        //console.log("Entré en service");
        return $http.get
        (
            urlCountries
        );
    }
});