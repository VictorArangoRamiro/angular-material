angular.module('MyApp')
.config(function($stateProvider, $urlRouterProvider)
{

    $urlRouterProvider.otherwise('/home');

    $stateProvider
    .state('material',
    {
        name: 'material',
        url: '/material',
        component: 'material'
    })
    .state('interlude',
    {
        name: 'interlude',
        url: '/interlude',
        component: 'interlude'
    })
    .state('welcome',
    {
        name: 'welcome',
        url: '/welcome',
        component: 'welcome'
    })
    .state('about',
    {
        name: 'about',
        url: '/about',
        component: 'about'
    })
    .state('registry',
    {
        name: 'registry',
        url: '/registry',
        component: 'registry'
    })
    .state('summoner',
    {
        name: 'summoner',
        url: '/summoner',
        component: 'summoner'
    });
});
/*.provider('getCountries', function($http, $q)
{
    var urlCountries = 'https://restcountries.eu/rest/v2/all';
    this.$get = function ($http, $q) 
    {
        return
        {
            callCountries: function()
            {
                var deferred = $q.defer();
                $http
                (
                    {
                        method: 'JSONP',
                        url: urlCountries
                    }
                )
                .success
                (
                    function (data)
                    {
                        deferred.resolve(data);
                        console.log(deferred.resolve(data););
                    }
                )
                .error
                (
                    function ()
                    {
                        deferred.reject('There was an error');
                    }
                )
                return deferred.promise;
            };
        }
    }
});*/