'use strict';

angular.module('MyApp')
.component
(
    'footer',
    {
        templateUrl:  'components/essential-components/footer.html',
    }
);