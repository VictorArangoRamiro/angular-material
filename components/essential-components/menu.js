'use strict';

angular.module('MyApp')
.component
(
    'menu',
    {
        templateUrl:  'components/essential-components/menu.html',
    }
);