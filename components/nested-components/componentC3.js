'use strict';

angular.module('MyApp')
.component
(
    'componentC3',
    {
        templateUrl:  'components/NestedComponents/componentC3.html',
           
        controller: function($scope)
        {
            
            //handle SendDown event
            $scope.$on
            (
                "SendDown",
                function (evt, data)
                {
                    $scope.Message = "Component 3 - Down: " + data;
                }
            );

            //emit SendUp event up
            $scope.OnClick = function (evt)
            {
                $scope.$emit("SendUp", "desde el 3");
            }

            //handle SendUp event
            $scope.$on
            (
                "SendUp",
                function (evt, data)
                {
                    $scope.Message = "Component 3 - Up: " + data;
                }
            );
        }
    }
);