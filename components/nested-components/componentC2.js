'use strict';

angular.module('MyApp')
.component
(
    'componentC2',
    {
        templateUrl:  'components/NestedComponents/componentC2.html',
           
        controller: function($scope)
        {
            
            //handle SendDown event
            $scope.$on
            (
                "SendDown",
                function (evt, data)
                {
                    $scope.Message = "Component 2 - Down: " + data;
                }
            );

            //emit SendUp event up
            $scope.OnClick = function (evt)
            {
                $scope.$broadcast("SendDown", "desde el 2");
                //$scope.$emit("SendUp", "desde el 2");
            }
            
            //emit SendUp event up
            $scope.OnClickEmit = function (evt)
            {
                $scope.$emit("SendUp", "desde el 2");
            }

            //handle SendUp event
            $scope.$on
            (
                "SendUp",
                function (evt, data)
                {
                    $scope.Message = "Component 2 - Up: " + data;
                }
            );
        }
    }
);