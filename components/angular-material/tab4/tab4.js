'use strict';

angular.module('MyApp')
.component
(
    'tab4',
    {
        templateUrl:  'components/angular-material/tab4/tab4.html',
        
        controller: function($scope)
        {
            $scope.currentNavItem = '';
            
            $scope.goto = function(page)
            {
                $scope.status = "Goto " + page;
            };
        }
    }
);