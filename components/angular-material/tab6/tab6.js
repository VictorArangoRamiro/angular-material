'use strict';

angular.module('MyApp')
.component
(
    'tab6',
    {
        templateUrl:  'components/angular-material/tab6/tab6.html',
        
        controller: function($scope, $log)
        {
            $scope.onSwipeLeft = function(ev, target)
            {
                alert('You swiped left!!');

                $log.log('Event Target: ', ev.target);
                $log.log('Event Current Target: ', ev.currentTarget);
                $log.log('Original Current Target: ', target.current);
            };

            $scope.onSwipeRight = function(ev, target)
            {
                alert('You swiped right!!');

                $log.log('Event Target: ', ev.target);
                $log.log('Event Current Target: ', ev.currentTarget);
                $log.log('Original Current Target: ', target.current);
            };
    
            $scope.onSwipeUp = function(ev, target)
            {
                alert('You swiped up!!');

                $log.log('Event Target: ', ev.target);
                $log.log('Event Current Target: ', ev.currentTarget);
                $log.log('Original Current Target: ', target.current);
            };

            $scope.onSwipeDown = function(ev, target)
            {
                alert('You swiped down!!');

                $log.log('Event Target: ', ev.target);
                $log.log('Event Current Target: ', ev.currentTarget);
                $log.log('Original Current Target: ', target.current);
            };
            
            $scope.items = [];
            for (var i = 0; i < 100; i++) 
            {
                $scope.items.push(i);
            }
        }
    }
);