'use strict';

angular.module('MyApp')
.component
(
    'material',
    {
        templateUrl:  'components/angular-material/menu.html',
           
        controller: function($scope)
        {
            $scope.currentNavItem = 'page1';

            $scope.goto = function(page)
            {
                $scope.status = "Goto " + page;
            };
        }
    }
);