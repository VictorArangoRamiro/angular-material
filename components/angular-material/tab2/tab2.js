'use strict';

angular.module('MyApp')
.component
(
    'tab2',
    {
        templateUrl:  'components/angular-material/tab2/tab2.html',
        controller: function($scope, $mdDialog)
        {
            
            // Contact Chip
            /*
            var self = this;
            self.querySearch = querySearch;
            self.allContacts = loadContacts();
            self.contacts = [self.allContacts[0]];
            self.filterSelected = true;
            
            function querySearch (query) {
               var results = query ?
               self.allContacts.filter(createFilterFor(query)) : [];
               return results;
            }

            function createFilterFor(query) {
               var lowercaseQuery = angular.lowercase(query);
               return function filterFn(contact) {
                  return (contact._lowername.indexOf(lowercaseQuery) != -1);;
               };
            }
            
            function loadContacts() {
               var contacts = [
                  'Roberto Karlos',
                  'Bob Crestor',
                  'Nigel Rick',
                  'Narayana Garner',
                  'Anita Gros',
                  'Megan Smith',
                  'Tsvetko Metzger',
                  'Hector Simek',
                  'James Roody'
               ];
               
               return contacts.map(function (c, index) {
                  var cParts = c.split(' ');
                  var contact = {
                     name: c,
                     email: cParts[0][0].toLowerCase() + '.' + cParts[1].toLowerCase()
                        + '@example.com',
                     image: 'http://lorempixel.com/50/50/people?' + index
                  };
                  contact._lowername = contact.name.toLowerCase();
                  return contact;
               });
            }*/
            $scope.openFromLeft = function()
            {
                $mdDialog.show
                (
                    $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Opening dialog')
                    .textContent('Hello! This is a description :p')
                    .ariaLabel('Dialog demo')
                    .ok('Nice!')
                    // or an element
                    .closeTo(angular.element(document.querySelector('#right')))
                );
            };
            
            
            // Divide
            var imagePath = 'img/person.png';
            $scope.messages = 
            [
                {
                    face : imagePath,
                    what: 'Brunch this weekend?',
                    who: 'Min Li Chan',
                    when: '3:08PM',
                    notes: " I'll be in your neighborhood doing errands"
                },
                {
                    face : imagePath,
                    what: 'Brunch this weekend?',
                    who: 'Min Li Chan',
                    when: '3:08PM',
                    notes: " I'll be in your neighborhood doing errands"
                }
            ];
        }
    }
);