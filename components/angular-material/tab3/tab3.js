'use strict';

angular.module('MyApp')
.component
(
    'tab3',
    {
        templateUrl:  'components/angular-material/tab3/tab3.html',
        
        controller: function($scope)
        {
            $scope.items =
            [
                'a',
                'b',
                'c'
            ];
            
            $scope.todos =
            [
                {   
                    title: 'a',
                    description: 'primera letra'
                },
                {   
                    title: 'b',
                    description: 'segunda letra'
                }
            ];
        }
    }
);