'use strict';

angular.module('MyApp')
.component
(
    'tab1',
    {
        templateUrl:  'components/angular-material/tab1/tab1.html',
        
        controller: function($scope)
        {
            
            //Chip - Remove
            $scope.readonly = false;
            $scope.fruitNames = ['Apple', 'Banana', 'Orange'];
            $scope.ngChangeFruitNames = angular.copy($scope.fruitNames);
            $scope.roFruitNames = angular.copy($scope.fruitNames);
            $scope.editableFruitNames = angular.copy($scope.fruitNames);
        }
    }
);