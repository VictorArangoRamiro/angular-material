'use strict';

angular.module('MyApp')
.component
(
    'tab5',
    {
        templateUrl:  'components/angular-material/tab5/tab5.html',
        
        controller: function($scope, $mdSidenav)
        {
            $scope.items = [1, 2, 3, 4, 5, 6, 7];
            $scope.selectedItem = undefined;

            $scope.getSelectedText = function ()
            {
                if ($scope.selectedItem !== undefined)
                {
                    return "You have selected: Item " + $scope.selectedItem;
                }
                else
                {
                    return "Please select an item";
                }
            };
            
            $scope.toggleLeft = buildToggler('left');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    }
        }
    }
);