'use strict';

angular.module('MyApp')
.component
(
    'message',
    {
        templateUrl:  'components/message.html',
        bindings:
        {
            //messageInput: '='
        },
        controller: function($scope, $rootScope)
        {
            this.onUpdate = function (evt)
            {
                $rootScope.$broadcast("Send", "Change!");
            }
            
            $scope.$on
            (
                "Send",
                function (evt, data)
                {
                    $scope.messageInput = data;
                }
            );            
        }
    }
);