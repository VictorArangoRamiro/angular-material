'use strict';

angular.module('MyApp')
.component
(
    'formComponent',
    {
        templateUrl:  'components/formComponent.html',
        bindings:
        {
            //mess: '='
        },
        controller: function($scope, $rootScope)
        {
            //this.messageInput = this.mess;
            
            this.onUpdate = function (evt)
            {
                $rootScope.$broadcast("Send", this.messageInput);
            }
            
            this.onUpdate2 = function (evt)
            {
                this.messageInput = $scope.messageInput;
            }
            
            $scope.$on
            (
                "Send",
                function (evt, data)
                {
                    $scope.messageInput = data;
                }
            );
        }
    }
);