'use strict';

angular.module('MyApp')
.component
(
    'registry',
    {
        templateUrl:  'components/interlude/registry.html',
           
        controller: function($scope, $rootScope, countriesService, $mdSidenav)
        {
            $scope.toggleLeft = buildToggler('left');
            function buildToggler(componentId)
            {
                return function()
                {
                    $mdSidenav(componentId).toggle();
                };
            }
            
            
            $scope.countries = [];
            $scope.getCountries = function()
            {
                countriesService.getCountriesData()
                .then
                (
                    function (response)
                    {
                        $scope.information = response.data;
                        //console.log($scope.information[0].name);
                        //console.log($scope.information.length);
                        
                        $scope.information.forEach
                        (
                            function(country)
                            {
                                //console.log(country.name);
                                $scope.countries.push(country.name);
                            }
                        );
                    }
                )
                .catch
                (
                    function (response)
                    {
                        alert('Ha ocurrido un error, código: ' + response.data['status'].status_code);
                    }
                )
                .finally
                (
                    function (response)
                    {
                        //console.log('Task finished in submit elo.');
                        //console.log($scope.countries);
                    }
                );
            }
            
            angular.element(document).ready(function ()
            {
                $scope.getCountries();
            });
        }
    }
);