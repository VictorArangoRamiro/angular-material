'use strict';

angular.module('MyApp')
.component
(
    'about',
    {
        templateUrl:  'components/interlude/about.html',
           
        controller: function($scope, $rootScope, $mdSidenav, webServicePerron)
        {
            
            
            $scope.toggleLeft = buildToggler('left');
            function buildToggler(componentId)
            {
                return function()
                {
                    $mdSidenav(componentId).toggle();
                };
            }
            
            $scope.webServiceFtoC = function()
            {
                webServicePerron.getDataFtoC($scope.fahrenheit)
                .then
                (
                    function (response)
                    {
                        $scope.convertedF = response.data['C'];
                    }
                )
                .catch
                (
                    function (response)
                    {
                        console.log('Ha ocurrido un error :(');
                    }
                )
                .finally
                (
                    function (response)
                    {
                        //Execute in the end.
                    }
                );
            }
            
            $scope.webServiceCtoF = function()
            {
                webServicePerron.getDataCtoF($scope.celsius)
                .then
                (
                    function (response)
                    {
                        $scope.convertedC = response.data['F'];
                    }
                )
                .catch
                (
                    function (response)
                    {
                        console.log('Ha ocurrido un error :(');
                    }
                )
                .finally
                (
                    function (response)
                    {
                        //Execute in the end.
                    }
                );
            }
        }
    }
);