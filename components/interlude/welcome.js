'use strict';

angular.module('MyApp')
.component
(
    'welcome',
    {
        templateUrl:  'components/interlude/welcome.html',
           
        controller: function($scope, $rootScope, $mdSidenav)
        {
            $scope.toggleLeft = buildToggler('left');

            function buildToggler(componentId)
            {
                return function()
                {
                    $mdSidenav(componentId).toggle();
                };
            }
        }
    }
);