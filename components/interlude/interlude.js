'use strict';

angular.module('MyApp')
.component
(
    'interlude',
    {
        templateUrl:  'components/interlude/interlude.html',
           
        controller: function($scope, $rootScope, $mdSidenav, $log, $sce)
        {
            
            $scope.pageActive = 1;
            $scope.currentPage = 'Home';
            $scope.toggleLeft = buildToggler('left');

            function buildToggler(componentId)
            {
                return function()
                {
                    $mdSidenav(componentId).toggle();
                };
            }
        }
    }
);