create database AngularMaterial

use AngularMaterial

create table Users
(
	UserID int PRIMARY KEY NOT NULL,
	UserName varchar (30) NOT NULL,
	UserLastName varchar (30) NOT NULL,
	UserEmail varchar (30) NOT NULL,
	UserPassword varchar (30) NOT NULL,
	UserSummoner varchar (30) NOT NULL
);

create table Summoner
(
	SummonerID int PRIMARY KEY NOT NULL,
	SummonerAPI_ID text NOT NULL,
	SummonerName varchar (30) NOT NULL,
	SummonerLevel int NOT NULL,
	SummonerIcon text NOT NULL,
	SummonerWins int NOT NULL,
	SummonerLosses int NOT NULL
);

create table SummonerFollowing
(
	FollowingID int PRIMARY KEY NOT NULL,
	UserID int FOREIGN KEY REFERENCES Users(UserID),
	SummonerID int FOREIGN KEY REFERENCES Summoner(SummonerID)
);

create table Champions
(
	ChampionID int PRIMARY KEY NOT NULL,
	Champion int NOT NULL,
	ChampionLane varchar (30) NOT NULL,
	ChampionRole varchar (30) NOT NULL,
	ChampionIcon text NOT NULL
);

create table ChampsRelation
(
	ChampsID int PRIMARY KEY NOT NULL,
	SummonerID int FOREIGN KEY REFERENCES Summoner(SummonerID),
	ChampionID int FOREIGN KEY REFERENCES Champions(ChampionID)
);

create table Matches
(
	MatchesID int PRIMARY KEY NOT NULL,
	MatchesType varchar (20),

);

insert into Users values
(
	1,
	'V�ctor Manuel',
	'Arango Ramiro',
	'muzik1421@gmail.com',
	'yolo',
	'Skelet0r'
);

select * from Users

drop table Users

drop table Summoner

drop table SummonerFollowing

drop table Champions